import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import LayoutScreen from './src/screens/LayoutScreen';
import ComponentsScreen from './src/screens/ComponentScreen';
import ListScreen from './src/screens/ListScreen';
import ImageScreen from './src/screens/ImageScreen';
import ImageDetail from './src/components/ImageDetail';
import CounterScreen from './src/screens/CounterScreen';
import ColorScreen from './src/screens/ColorScreen';
import SquareScreen from './src/screens/SquareScreen';

const navigator = createStackNavigator({
  Home: HomeScreen,
  Layout: LayoutScreen,
  Components:ComponentsScreen,
  List:ListScreen,
  Image:ImageScreen,
  Imagedetail:ImageDetail,
  Counter:CounterScreen,
  Color:ColorScreen,
  Square:SquareScreen
},
{
  initialRouteName:'Home',
  defaultNavigationOptions:{
    title:'App'
  }
}

);

export default createAppContainer(navigator);
