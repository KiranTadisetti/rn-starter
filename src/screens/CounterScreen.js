import React, {useState}from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';


//useState is the hook 
const CounterScreen=()=>{
    const [counter,setCounter]=useState(0);  //[] array destructering
    return (
        <View>
            
            <Button title='Increase' onPress={()=>{
                setCounter(counter+1)
            }}/>
            <Button title='Decrease' onPress={ ()=>{
                setCounter(counter-1)
            }
            }/>
            <Text>Currebt Count: {counter}</Text>
        </View>
    );
}
CounterScreen.navigationOptions={
    title:'Counter'
}

const styles =StyleSheet.create({

});

export default CounterScreen;