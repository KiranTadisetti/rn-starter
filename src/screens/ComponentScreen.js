import React from 'react';
import {Text,StyleSheet,View} from 'react-native';

const ComponentsScreen = () => {
    const name="Kiran!";
    return (
        <View>
        <Text style={styles.textStyle}>Getting started with React Native</Text>
        <Text style={styles.subHeaderStyle}>This is {name}</Text>
        </View>
    );
};
ComponentsScreen.navigationOptions = {
    title: 'Component'
  };
const styles = StyleSheet.create({
    textStyle:{
        fontSize: 30
    },
    subHeaderStyle:{
        fontSize:20
    
    }
});

export default ComponentsScreen;