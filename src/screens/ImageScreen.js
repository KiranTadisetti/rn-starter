 import React from 'react';
 import {View,Text,StyleSheet} from 'react-native';
import ImageDetail from '../components/ImageDetail'


 const ImageScreen=()=>{
    return(
        <View>
            <ImageDetail title="forest" imageSrc={require('../../assets/forest.jpg')} imageScore='10'/>
            <ImageDetail title="Beach" imageSrc={require('../../assets/beach.jpg')} imageScore='7'/>
            <ImageDetail title="Mountain" imageSrc={require('../../assets/mountain.jpg')} imageScore='4'/>
        </View>
    );
 };

ImageScreen.navigationOptions= {
    title:'Image'
}

 const styles=StyleSheet.create({

 });

 export default ImageScreen;