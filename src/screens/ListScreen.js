import React from 'react'
import { View, Text, StyleSheet, FlatList } from 'react-native';

const ListScreen = () => {
    const friends=[
        {name: 'Friend1',Age:'20'},
        {name: 'Friend2',Age:'21'},
        {name: 'Friend3',Age:'22'},
        {name: 'Friend4',Age:'23'},
        {name: 'Friend5',Age:'24'},
        {name: 'Friend6',Age:'25'},
        {name: 'Friend7',Age:'26'},
        {name: 'Friend8',Age:'27'},
        {name: 'Friend9',Age:'28'},
        {name: 'Friend10',Age:'29'},
        {name: 'Friend11',Age:'30'}
    ]
    return (
        <View>
            <Text> List Screen</Text>
            <FlatList
                //horizontal={true}
                showsVerticalScrollIndicator={false}
                keyExtractor={(friend) => friend.name}
                data={friends}
                renderItem={({ item }) => {
                    return <Text style={styles.textStyle}>my name is {item.name} & my age is {item.Age}</Text>
                }}>

            </FlatList>
        </View>
    );
}

const styles = StyleSheet.create(
    {
        textStyle:{
            marginVertical:50
        }
    }
);
ListScreen.navigationOptions = {
    title: 'List'
  };
export default ListScreen;